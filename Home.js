import React from 'react'
import {Text, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'

const Home = ()=>{
    const goToProfile = ()=>{
        Actions.profile()
    }
    return(
        <TouchableOpacity onPress={goToProfile}>
            <Text>Mi Perfil</Text>
        </TouchableOpacity>
    )
}

export default Home