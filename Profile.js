import React from 'react'
import {Text, TouchableOpacity} from 'react-native'
import {Actions} from 'react-native-router-flux'

const Profile = ()=>{
    const goToHome = ()=>{
        Actions.home()
    }
    return(
        <TouchableOpacity onPress={goToHome}>
            <Text>Hola mundo</Text>
        </TouchableOpacity>
    )
}

export default Profile